/* jshint esversion:9 */
const gameBoard = document.getElementById("gameContainer");
const newGameButton = document.getElementById("newGame");
let playerTurn = 1;
const playerOne = document.getElementById("playerOne");
const playerTwo = document.getElementById("playerTwo");
const playerOneArea = document.getElementById("playerOneScore");
const playerTwoArea = document.getElementById("playerTwoScore");
let playerOneScore = parseInt(playerOneArea.innerHTML);
let playerTwoScore = parseInt(playerTwoArea.innerHTML);
const playerOneColor = "firebrick";
const playerTwoColor = "blue";
let totalPieces = 0;
let turns = 1;
const displayArea = document.getElementById("messageArea");

const emptyGamePlay = {
    col0: ["empty", "empty", "empty", "empty", "empty", "empty"],
    col1: ["empty", "empty", "empty", "empty", "empty", "empty"],
    col2: ["empty", "empty", "empty", "empty", "empty", "empty"],
    col3: ["empty", "empty", "empty", "empty", "empty", "empty"],
    col4: ["empty", "empty", "empty", "empty", "empty", "empty"],
    col5: ["empty", "empty", "empty", "empty", "empty", "empty"],
    col6: ["empty", "empty", "empty", "empty", "empty", "empty"],
};


let gamePlay = JSON.parse(JSON.stringify(emptyGamePlay));

const checkHorizontalBackwards = function(col, row, playerNum){
    const gameKeys = Object.keys(gamePlay);
    let count = 0;
    let loopEnd = 0;
    if (col >= 4) loopEnd = col - 3;
    for (let index = col; index >= loopEnd; index--) {
        if (gamePlay[gameKeys[index]][row] === playerNum) count++;
    }
    if (count === 4) {
        newGameButton.style.visibility = "visible";
        gameBoard.style.pointerEvents = "none";
        displayArea.innerHTML = '   Player ' + playerNum + ' Wins!!';
        if (playerNum === "1"){
            playerOneScore++;
            playerOneArea.innerHTML = playerOneScore;
        }else{
            playerTwoScore++;
            playerTwoArea.innerHTML = playerTwoScore;
        }
        //window.setTimeout(resetGame, 4000);
        
    }
};
const checkHorizontalForwards = function(col, row, playerNum){
    const gameKeys = Object.keys(gamePlay);
    let count = 0;
    let loopEnd = 6;
    if (col<=3) loopEnd = col + 3;
    for (let index = col; index <= loopEnd; index++) {
        if (gamePlay[gameKeys[index]][row] === playerNum) count++;
    }
    if (count === 4) {newGameButton.style.visibility = "visible";
        newGameButton.style.visibility = "visible";
        gameBoard.style.pointerEvents = "none";
        displayArea.innerHTML = '    Player ' + playerNum + ' Wins!!';
        if (playerNum === "1"){
            playerOneScore++;
            playerOneArea.innerHTML = playerOneScore;
        }else{
            playerTwoScore++;
            playerTwoArea.innerHTML = playerTwoScore;
        }
       // window.setTimeout(resetGame, 4000);
       
    }
};
const checkVerticalBackwards = function(col, row, playerNum){
    const gameKeys = Object.keys(gamePlay);
    let count = 0;
    let loopEnd = 0;
    if (row >= 4) loopEnd = row - 3;
    for (let index = row; index >= loopEnd; index--) {
        if (gamePlay[gameKeys[col]][index] === playerNum) count++;
    }
    if (count === 4) {
        newGameButton.style.visibility = "visible";
        gameBoard.style.pointerEvents = "none";
        displayArea.innerHTML = '    Player ' + playerNum + ' Wins!!';
        if (playerNum === "1"){
            playerOneScore++;
            playerOneArea.innerHTML = playerOneScore;
        }else{
            playerTwoScore++;
            playerTwoArea.innerHTML = playerTwoScore;
        }
        //window.setTimeout(resetGame, 4000);
        
    }
};
const checkVerticalForwards = function(col, row, playerNum){
    const gameKeys = Object.keys(gamePlay);
    let count = 0;
    let loopEnd = 0;
    if (row <= 3) loopEnd = row + 3;
    for (let index = row; index <= loopEnd; index++) {
        if (gamePlay[gameKeys[col]][index] === playerNum) count++;
    }
    if (count === 4) {
        newGameButton.style.visibility = "visible";
        gameBoard.style.pointerEvents = "none";
        displayArea.innerHTML = '    Player ' + playerNum + ' Wins!!';
        if (playerNum === "1"){
            playerOneScore++;
            playerOneArea.innerHTML = playerOneScore;
        }else{
            playerTwoScore++;
            playerTwoArea.innerHTML = playerTwoScore;
        }
        //window.setTimeout(resetGame, 4000);
        
    }
};
const checkDiagonalUpRight = function(col, row, playerNum){
    const gameKeys = Object.keys(gamePlay);
    let count = 0;
    let loopEnd = 6;
    if (col <=3) loopEnd = col + 3;
    for (let index = col; index <= loopEnd; index++){
        if (gamePlay[gameKeys[index]][row]=== playerNum) {
            count++;
            row++;
        }
    }
    if (count === 4) {
        newGameButton.style.visibility = "visible";
        gameBoard.style.pointerEvents = "none";
        displayArea.innerHTML = '    Player ' + playerNum + ' Wins!!';
        if (playerNum === "1"){
            playerOneScore++;
            playerOneArea.innerHTML = playerOneScore;
        }else{
            playerTwoScore++;
            playerTwoArea.innerHTML = playerTwoScore;
        }
       // window.setTimeout(resetGame, 4000);
        
    }
    
};
const checkDiagonalDownRight = function(col, row, playerNum){
    const gameKeys = Object.keys(gamePlay);
    let count = 0;
    let loopEnd = 6;
    if (col <=3) loopEnd = col + 3;
    for (let index = col; index <= loopEnd; index++){
        if (gamePlay[gameKeys[index]][row]=== playerNum) {
            count++;
            row--;
        }
    }
    if (count === 4) {
        newGameButton.style.visibility = "visible";
        gameBoard.style.pointerEvents = "none";
        displayArea.innerHTML = '    Player ' + playerNum + ' Wins!!';
        if (playerNum === "1"){
            playerOneScore++;
            playerOneArea.innerHTML = playerOneScore;
        }else{
            playerTwoScore++;
            playerTwoArea.innerHTML = playerTwoScore;
        }
        //window.setTimeout(resetGame, 4000);
        
    }
    
};
const checkDiagonalUpLeft = function(col, row, playerNum){
    const gameKeys = Object.keys(gamePlay);
    let count = 0;
    let loopEnd = 6;
    if (col >=3) loopEnd = col - 3;
    for (let index = col; index >= loopEnd; index--){
        if (gamePlay[gameKeys[index]][row]=== playerNum) {
            count++;
            row++;
        }
    }
    if (count === 4) {
        newGameButton.style.visibility = "visible";
        gameBoard.style.pointerEvents = "none";
        displayArea.innerHTML = '    Player ' + playerNum + ' Wins!!';
        if (playerNum === "1"){
            playerOneScore++;
            playerOneArea.innerHTML = playerOneScore;
        }else{
            playerTwoScore++;
            playerTwoArea.innerHTML = playerTwoScore;
        }
        //window.setTimeout(resetGame, 4000);
        
    }
    
};
const checkDiagonalDownLeft = function(col, row, playerNum){
    const gameKeys = Object.keys(gamePlay);
    let count = 0;
    let loopEnd = 6;
    if (col >=3) loopEnd = col - 3;
    for (let index = col; index >= loopEnd; index--){
        if (gamePlay[gameKeys[index]][row]=== playerNum) {
            count++;
            row--;
        }
    }
    if (count === 4) {
        newGameButton.style.visibility = "visible";
        gameBoard.style.pointerEvents = "none";
        displayArea.innerHTML = '    Player ' + playerNum + ' Wins!!';
        if (playerNum === "1"){
            playerOneScore++;
            playerOneArea.innerHTML = playerOneScore;
        }else{
            playerTwoScore++;
            playerTwoArea.innerHTML = playerTwoScore;
        }
        //window.setTimeout(resetGame, 4000);
        
    }
    
};
const checkForWin = function(col, row, playerNum, turns) {
    console.log(`Total Game Pieces = ${totalPieces}`);
    if (totalPieces === 42) {
        newGameButton.style.visibility = "visible";
        gameBoard.style.pointerEvents = "none";
        displayArea.innerHTML = "Game Tied";
        
    } else if (((col+1)-4) >= 0 ) {
       checkHorizontalBackwards(col, row, playerNum);
   }else if((col+4) <= 7) {
       checkHorizontalForwards(col, row, playerNum);
   }
   if (((row+1)-4) >= 0){
       checkVerticalBackwards(col, row, playerNum);
   }else if((row+4) <= 6){
       checkVerticalForwards(col, row, playerNum);
   }
   checkDiagonalUpRight(col, row, playerNum);
   checkDiagonalDownRight(col, row, playerNum);
   checkDiagonalUpLeft(col, row, playerNum);
   checkDiagonalDownLeft(col, row, playerNum);
};



const findEmpty = function(colClicked) {
    return gamePlay[colClicked].indexOf("empty");
};
const placeGamePiece = function(colNum, rowNum, player, turns) {
    
    let gamePiece = document.querySelector("div[data-row='" + rowNum + "'][data-col='" + colNum + "']");
    
        if (player === 1) {
            gamePiece.classList.add("playerOnePiece");
            totalPieces++;
            gamePiece.dataset.player = "One";
            gamePlay["col" + colNum][rowNum] = "1";
            playerTwo.classList.add("neon");
            playerOne.classList.remove("neon");
            checkForWin(parseInt(colNum), parseInt(rowNum), "1", turns);
            playerTurn = 2;
            
        } else {
            gamePiece.classList.add("playerTwoPiece");
            totalPieces++;
            gamePiece.dataset.player = "Two";
            gamePlay["col" + colNum][rowNum] = "2";
            playerOne.classList.add("neon");
            playerTwo.classList.remove("neon");
            checkForWin(parseInt(colNum), parseInt(rowNum), "2", turns);
            playerTurn = 1;
            
        }
    

};

const playGame = function() {
    let cell = event.target;

    let colClicked = "col" + cell.dataset.col;
    if (cell.id !== "gameContainer") {
        if (playerTurn === 1) {
            placeGamePiece(cell.dataset.col, findEmpty(colClicked), 1, turns);
            turns++;
        } else {
            placeGamePiece(cell.dataset.col, findEmpty(colClicked), 2, turns);
            turns++;
        }
    }
};
//Build Game Board
const buildGameBoard = function(){

    let cellNum = 5;
    for (let row = 0; row < 6; row++) {
        
        for (let col = 0; col < 7; col++) {
            let newDiv = document.createElement("div");
            newDiv.className = "cell";
            newDiv.setAttribute("data-row", cellNum);
            newDiv.setAttribute("data-col", col);
            newDiv.setAttribute("data-player", "empty");
            gameBoard.appendChild(newDiv);
            
        }
        cellNum--;
    }
};
buildGameBoard();
function resetGame() {
    gameBoard.innerHTML="";
    displayArea.innerHTML="";
    newGameButton.style.visibility = "hidden";
    buildGameBoard();
    gamePlay = JSON.parse(JSON.stringify(emptyGamePlay));
    gameBoard.style.pointerEvents= "auto";
    playerOne.classList.add("neon");
    playerTwo.classList.remove("neon");
    playerTurn = 1;
    totalPieces = 0;
}
gameBoard.addEventListener("click", playGame);