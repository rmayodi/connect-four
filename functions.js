let checkBackwardsHorizontal = function(col, row, playerNum) {
    const gameKeys = Object.keys(gamePlay);
    let count = 0;
    let loopEnd = 0;
    if (col >= 4) loopEnd = col - 3;
    for (let index = col; index >= loopEnd; index--) {
        if (gamePlay[gameKeys[index]][row] === playerNum) count++;
    }
    if (count === 4) {
        console.log("Player " + playerNum + " Wins!");
        //window.setTimeout(resetGame, 3000);
    }

};
let checkBackwardsVertical = function(col, row, playerNum) {
    const gameKeys = Object.keys(gamePlay);
    let count = 0;
    let loopEnd = 0;
    if (row >= 4) loopEnd = row - 3;
    for (let index = row; index >= loopEnd; index--) {
        if (gamePlay[gameKeys[col]][index] === playerNum) count++;
    }
    if (count === 4) {
        console.log("Player " + playerNum + " Wins!");
    }
};

let checkRightUpDiagonal = function(col, row, playerNum) {
    const gameKeys = Object.keys(gamePlay);
    let count = 1;
    for (let index = 0; index < 3; index++) {
        if (gamePlay[gameKeys[col + 1]][row + 1] === playerNum) {
            count++;
            col--;
            console.log(col);
            row--;
            console.log(row);
        }
    }
    if (count === 4) {
        console.log("Player " + playerNum + " Wins!");
        //window.setTimeout(resetGame, 3000);
    }
};

let checkRightDownDiagonal = function(col, row, playerNum) {
    const gameKeys = Object.keys(gamePlay);
    let count = 1;
    for (let index = 0; index < 3; index++) {
        if (gamePlay[gameKeys[col - 1]][row - 1] === playerNum) {
            count++;
            col--;
            console.log(col);
            row--;
            console.log(row);
        }
    }
    if (count === 4) {
        console.log("Player " + playerNum + " Wins!");
        //window.setTimeout(resetGame, 3000);
    }
};

let checkLeftUpDiagonal = function(col, row, playerNum) {
    const gameKeys = Object.keys(gamePlay);
    let count = 1;
    for (let index = 0; index < 3; index++) {
        if (gamePlay[gameKeys[col - 1]][row + 1] === playerNum) {
            count++;
            col--;
            console.log(col);
            row--;
            console.log(row);
        }
    }
    if (count === 4) {
        console.log("Player " + playerNum + " Wins!");
        //window.setTimeout(resetGame, 3000);
    }
};

let checkLeftDownDiagonal = function(col, row, playerNum) {
    const gameKeys = Object.keys(gamePlay);
    let count = 1;
    for (let index = 0; index < 3; index++) {
        if (gamePlay[gameKeys[col - 1]][row - 1] === playerNum) {
            count++;
            col--;
            console.log(col);
            row--;
            console.log(row);
        }
    }
    if (count === 4) {
        console.log("Player " + playerNum + " Wins!");
        //window.setTimeout(resetGame, 3000);
    }
};
let checkDiagonalUpRight = function(col, row, playerNum){
    const gameKeys = Object.keys(gamePlay);
    let count = 0;
    let rowCount = 0;
    //Check up Right
    if ((col+1) <= 6 && (row+1) <= 5){
        for (let colCount = 0; colCount <= 6; colCount++){
            if (gamePlay[gameKeys[colCount]][rowCount] === playerNum){
                count++;
            }else{
                break;
            }
            rowCount++;
            
        }
        if (count === 4) {
            return true;
        }else {
            return false;
        }
    }
    // //Check down Right
    // if ((col+1) <= 6 && (row-1) >= 0){
    //     for (let colCount = col; colCount <= 6; colCount++){
    //         for(let rowCount = row; rowCount >= 0; rowCount--){
    //             if (gamePlay[gameKeys[colCount]][rowCount] === playerNum){
    //                 count++;
    //             }else{
    //                 break;
    //             }
    //         }
    //     }
    //     if (count === 4) {
    //         return true;
    //     }else {
    //         return false;
    //     }
    // }
    //Check up Left
    if ((col-1) >= 0 && (row+1) <= 5){
        for (let colCount = col; colCount >= 0; colCount--){
            for(let rowCount = row; rowCount <= 5; rowCount++){
                if (gamePlay[gameKeys[colCount]][rowCount] === playerNum){
                    count++;
                }else{
                    return false;
                }
            }
        }
        if (count === 4) return true;
    }
    //Check down Left
    if ((col-1) >= 0 && (row-1) >= 0){
        for (let colCount = col; colCount >= 0; colCount--){
            for(let rowCount = row; rowCount >= 0; rowCount--){
                if (gamePlay[gameKeys[colCount]][rowCount] === playerNum){
                    count++;
                }else{
                    return false;
                }
            }
        }
        if (count === 4) return true;
    }
};
let checkDiagonalUpLeft = function (col, row, playerNum){
    const gameKeys = Object.keys(gamePlay);
    let count = 0;
    if ((col-1) >= 0 && (row+1) <= 5){
        for (let colCount = 0; colCount <= 6; colCount++){
            if (gamePlay[gameKeys[colCount]][rowCount] === playerNum){
                count++;
            }else{
                break;
            }
            rowCount++;
            
        }
        if (count === 4) {
            return true;
        }else {
            return false;
        }
    }

};

// // if (turns < 6) {
//     //     return false;
//     // } else {
//         console.log(" row num = " + row + " col num = " + col + " player = " + playerNum);
//         const gameKeys = Object.keys(gamePlay);
//         if (col >= 3) {
//             if (gamePlay[gameKeys[col - 1]][row] === playerNum) {
//                 checkBackwardsHorizontal(col, row, playerNum);
//             }
//         }
//         if (row >= 3) {
//             if (gamePlay[gameKeys[col]][row - 1] === playerNum) {
//                 if (checkBackwardsVertical(col, row, playerNum) === true) {
//                     //window.setTimeout(resetGame, 3000);
//                 } 
//             }
//         }
//     //}
//     if (checkDiagonalUpRight(col, row, playerNum) === true){
//         alert("player " + playerNum + " Wins! Diagonal Up RIght");
//     }
//     if (checkDiagonalDownRight(col, row, playerNum)=== true){
//         alert("player " + playerNum + " Wins! Diagonal Down Right");
//     }
//         // if (col <= 3 && row < 3) {
//         //     if (gamePlay[gameKeys[col + 1]][row + 1] === playerNum) {
//         //         checkRightUpDiagonal(col, row, playerNum);
//         //     }
//         // }
//         // if (col <= 3 && row >= 3) {
//         //     if (col === 0) {
//         //         if (gamePlay[gameKeys[col]][row - 1] === playerNum) {
//         //             checkRightDownDiagonal(col, row, playerNum);
//         //         }
//         //     } else if (gamePlay[gameKeys[col - 1]][row - 1] === playerNum) {
//         //         checkRightDownDiagonal(col, row, playerNum);
//         //     }
//         // }
//         // if (col >= 3 && row < 3) {
//         //     if (col === 0) {
//         //         if (gamePlay[gameKeys[col]][row - 1] === playerNum) {
//         //             checkLeftUpDiagonal(col, row, playerNum);
//         //         }
//         //     } else if (gamePlay[gameKeys[col - 1]][row + 1] === playerNum) {
//         //         checkLeftUpDiagonal(col, row, playerNum);
//         //     }
//         // }
//         // if (col >= 3 && row >= 3) {
//         //     if (col === 0) {
//         //         if (gamePlay[gameKeys[col]][row - 1] === playerNum) {
//         //             checkLeftDownDiagonal(col, row, playerNum);
//         //         }
//         //     } else if (gamePlay[gameKeys[col - 1]][row - 1] === playerNum) {
//         //         checkLeftDownDiagonal(col, row, playerNum);
//         //     }
//         // }
//     //}