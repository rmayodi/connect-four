/*          Connect 4 Project Outline   

            *** HTML AND CSS THINGS TO DO *****

    1- Create layout: We decided to use a CSS Grid layout.
    2- Generate game board in JS.
    3- A way to identify whose move it is player one or two.


            **** JAVASCRIPT THINGS TO DO ****
    1- Allow player to click on game board to select slot for disc.(DONE!)
    2- Ignore clicks on game board itself, only act when circle is clicked
    3- Prevent players from clicking on an already occupied circle.
    4- Determine Wins:
        a- Diagonal
        b- Vertical
        c- Horizontal

        Object GamePlay:

        gamePlay: {
                c1: one, one, two, r4, r5, r6
                c2: two, one, two, r4, r5, r6
                c3: r1, r2, r3, r4, r5, r6
                c4: r1, r2, r3, r4, r5, r6
                c5: r1, r2, r3, r4, r5, r6
                c6: r1, r2, r3, r4, r5, r6
        }
         c1      c2      c3      c4      c5      c6      c7
 r6       *       *       *       *       *       *       *
 r5       *       *       *       *       *       *       *
 r4       *       *       *       *       *       *       *
 r3       TWO     Two     *       *       *       *       *
 r2       ONE     One     *       *       *       *       *
 r1       ONE     Two     *       *       *       *       *

        Object GameBoard:

        gameBoard: {
                c1: one, one, two, r4, r5, r6
                c2: two, one, two, r4, r5, r6
                c3: r1, r2, r3, r4, r5, r6
                c4: r1, r2, r3, r4, r5, r6
                c5: r1, r2, r3, r4, r5, r6
                c6: r1, r2, r3, r4, r5, r6
        }
         c1      c2      c3      c4      c5      c6      c7
 r6       *       *       *       *       *       *       *
 r5       *       *       *       *       *       *       *
 r4       *       *       *       *       *       *       *
 r3       TWO     Two     *       *       *       *       *
 r2       ONE     One     *       *       *       *       *
 r1       ONE     Two     *       *       *       *       *
        
        
        */