/* jshint esversion:6 */
//Resources: GitHub nruffilo/LearnToCode github.com/nruffilo/LearnToCode/blob/master/javascript/connect_four.hmtl
let gameBoard = document.getElementById("gameContainer");
let playerTurn = 1;
let playerOne = document.getElementById("playerOne");
let playerTwo = document.getElementById("playerTwo");
const playerOneColor = "firebrick";
const playerTwoColor = "blue";
let totalPieces = 0;
let turns = 1;
let results = document.createElement("p");
let header = document.getElementById("header");


let gamePlay = {
    col0: ["empty", "empty", "empty", "empty", "empty", "empty"],
    col1: ["empty", "empty", "empty", "empty", "empty", "empty"],
    col2: ["empty", "empty", "empty", "empty", "empty", "empty"],
    col3: ["empty", "empty", "empty", "empty", "empty", "empty"],
    col4: ["empty", "empty", "empty", "empty", "empty", "empty"],
    col5: ["empty", "empty", "empty", "empty", "empty", "empty"],
    col6: ["empty", "empty", "empty", "empty", "empty", "empty"],
};


let rightwards = function(playerNum, gameKeys, playerSide) {
    if ((gamePlay[gameKeys[col + 1]][row] === playerNum) && (gamePlay[gameKeys[col + 2]][row] === playerNum) && (gamePlay[gameKeys[col + 3]][row] === playerNum)) {
        results.innerHTML = `<strong>"Player ${playerNum} Wins Across!"</strong>`;
        playerSide.appendChild(results);
        window.setTimeout(resetGame, 4000);
        return true;
    }
};

let upwards = function(playerNum, gameKeys, playerSide) {
    if ((gamePlay[gameKeys[col]][row + 1] === playerNum) && (gamePlay[gameKeys[col]][row + 2] === playerNum) && (gamePlay[gameKeys[col]][row + 3] === playerNum)) {
        results.innerHTML = `<strong>"Player ${playerNum} Wins Down!"</strong>`;
        playerSide.appendChild(results);
        window.setTimeout(resetGame, 4000);
        return true;
    }
};

let diagonalUpwards = function(row, playerNum, gameKeys, playerSide) {
    let count = false;
    let rowLoopEnd = row + 3;
    for (colIndex = 0; colIndex <= 3; colIndex++) {
        for (rowIndex = 0; rowIndex <= rowLoopEnd; rowIndex++) {
            if (gamePlay[gameKeys[colIndex]][rowIndex] === playerNum) {
                if ((gamePlay[gameKeys[colIndex + 1]][rowIndex + 1] === playerNum) && (gamePlay[gameKeys[colIndex + 2]][rowIndex + 2] === playerNum) && (gamePlay[gameKeys[colIndex + 3]][rowIndex + 3] === playerNum)) {
                    count = true;
                }
            }
        }
    }
    if (count === true) {
        results.innerHTML = `<strong>"Player ${playerNum} Wins Diagonally!!"</strong>`;
        playerSide.appendChild(results);
        window.setTimeout(resetGame, 4000);
        return true;
    }
};
let diagonalDownwards = function(row, playerNum, gameKeys, playerSide) {
    let count = false;
    let rowLoopEnd = row - 3;
    for (colIndex = 0; colIndex <= 3; colIndex++) {
        for (rowIndex = 5; rowIndex >= rowLoopEnd; rowIndex--) {
            if (gamePlay[gameKeys[colIndex]][rowIndex] === playerNum) {
                if ((gamePlay[gameKeys[colIndex + 1]][rowIndex - 1] === playerNum) && (gamePlay[gameKeys[colIndex + 2]][rowIndex - 2] === playerNum) && (gamePlay[gameKeys[colIndex + 3]][rowIndex - 3] === playerNum)) {
                    count = true;
                }
            }
        }
    }
    if (count === true) {
        results.innerHTML = `<strong>"Player ${playerNum} Wins Diagonally!"</strong>`;
        playerSide.appendChild(results);
       
        window.setTimeout(resetGame, 4000); // This line resets game commemented out for debugging
        return true;
    }
};

let checkForWin = function(playerNum, turns, playerSide) {
    
    const gameKeys = Object.keys(gamePlay); //using to getting col length
    const gameValues = Object.values(gamePlay)[0]; // using zero to getting row length
    if (turns < 7) {
        return false;
    } else {
        for (col = 0; col < gameKeys.length; col++) {
            for (row = 0; row < gameValues.length; row++) {
                if (gamePlay[gameKeys[col]][row] === playerNum) {
                    rightwards(playerNum, gameKeys, playerSide);
                    upwards(playerNum, gameKeys, playerSide);
                    if (row <= 2) {
                        diagonalUpwards(row, playerNum, gameKeys, playerSide);
                    }
                    if (row >= 3) {
                        diagonalDownwards(row, playerNum, gameKeys, playerSide);
                    }
                }
            }
        }
    }
};



let findEmpty = function(colClicked) {
    return gamePlay[colClicked].indexOf("empty");
};

let placeGamePiece = function(colNum, rowNum, player, turns) {
    //console.log("turn " + turns);
    let gamePiece = document.querySelector("div[data-row='" + rowNum + "'][data-col='" + colNum + "']");
    if (totalPieces === 42) {
        results.innerHTML = `<strong>"Game Tied"</strong>`;
        header.appendChild(results);
      
    } else {

        if (player === 1) {
            gamePiece.classList.add("playerOnePiece");
            //gamePiece.style.background = playerOneColor;
            gamePiece.dataset.player = "One";
            gamePlay["col" + colNum][rowNum] = "1";
            playerTwo.classList.add("neon");
            playerOne.classList.remove("neon");
            checkForWin("1", turns, playerOne);
            playerTurn = 2;
            totalPieces++;
        } else {
            gamePiece.classList.add("playerTwoPiece");
           
            gamePiece.dataset.player = "Two";
            gamePlay["col" + colNum][rowNum] = "2";
            playerOne.classList.add("neon");
            playerTwo.classList.remove("neon");
            checkForWin("2", turns, playerTwo); 
            playerTurn = 1;
            totalPieces++;
        }
    }

};

let playGame = function() {
    let cell = event.target;

    let colClicked = "col" + cell.dataset.col;
    if (cell.id !== "gameContainer") {
        if (playerTurn === 1) {
            placeGamePiece(cell.dataset.col, findEmpty(colClicked), 1, turns);
            turns++;
        } else {
            placeGamePiece(cell.dataset.col, findEmpty(colClicked), 2, turns);
            turns++;
        }
    }
};
//Build Game Board
let cellNum = 5;
for (let row = 0; row < 6; row++) {

    for (let col = 0; col < 7; col++) {
        let newDiv = document.createElement("div");
        newDiv.className = "cell";
        newDiv.setAttribute("data-row", cellNum);
        newDiv.setAttribute("data-col", col);
        newDiv.setAttribute("data-player", "empty");
        gameBoard.appendChild(newDiv);

    }
    cellNum--;
}

function resetGame() {
    window.location.reload(true); //reloads page to refresh to beginning
}
gameBoard.addEventListener("click", playGame);